[![official project](http://jb.gg/badges/official-plastic.svg)](https://confluence.jetbrains.com/display/ALL/JetBrains+on+GitHub)

This introduction version is adapted from the official 
[Jetbrains Kotlin-Workshop](https://github.com/JetBrains/kotlin-workshop) for your convenience.
The goal is to pass the failing unit tests in the `tests` directory of the `student` module by implementing the relevant
parts in the `tasks` directory. It's possible you have to change a Java class or a Kotlin class, or to write new code altogether.

## Kotlin Workshop

![Kotlin Logo](kotlinlogo.png)

### Structure

This workshop is divided into two parts

* Introduction to Kotlin
* Advanced Kotlin

It consists of 

* **Instructor Project**: Used to explain concepts, with existing code samples
* **Student Project**: For students to complete

The Instructor Project has Introduction and Advanced modules included. While the Introduction 
has a lot of pre-built examples, it also serves as a canvas to show more code while teaching. However, please make sure that if you're 
a contributing author to this project, that you *do not* check-in any ad-hoc code. It's also recommended to use Scratch files in IntelliJ IDEA
for this purpose.

### Authors

This workshop includes material from the following authors:

* [Hadi Hariri](https://github.com/hhariri)
* [Svetlana Isakova](https://github.com/svtk)
* [Dmitry Jemerov](https://github.com/yole)
* [Roman Elizarov](https://github.com/elizarov)

### License and Usage

The material provided in this repository, including but not limited to source code, presentations, notes and exercises is Copyright (c) 2017 JetBrains, and is provided as-is. 

![JetBrains Logo](jetbrainslogo.png)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.


JetBrains and Kotlin are Registered Trademarks of their respective trademark holders. 

#### Contributions

We welcome all contributions to this workshop. If you feel you'd like to contribute something, create a fork and make a pull request. Bug fixes, comments, suggestions, all welcome. 

If you're contributing code or samples, please send a pull request. If you'd like to update the slides or questions, please contact [Hadi Hariri](https://github.com/hhariri).  

#### Usage and endorsements 
 
You are welcome to use the material in this workshop provided you abide by the corresponding license agreement. Additionally 

* By using this material, with or without any modification, JetBrains does not endorse any workshop or training that you may offer.
* By using this material, with or without any modification, you may not use JetBrain's name to promote your workshop or training without prior written consent. 
* By using this material, provided there are no modifications, you may indicate that you are using JetBrains Kotlin Workshop Material.

If you're interested in becoming an official JetBrains Training Partner, please [contact us](https://www.jetbrains.com/company/partners/become_a_partner.html) 
