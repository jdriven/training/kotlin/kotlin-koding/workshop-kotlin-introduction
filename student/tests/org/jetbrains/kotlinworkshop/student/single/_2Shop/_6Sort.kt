package org.jetbrains.kotlinworkshop.student.single._2Shop

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class _6Sort {
    @Test
    fun testGetCustomersSortedByNumberOfOrders() {
        assertEquals(sortedCustomers, shop.getCustomersSortedByNumberOfOrders())
    }
}
