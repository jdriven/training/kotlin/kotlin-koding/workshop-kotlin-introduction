package org.jetbrains.kotlinworkshop.student.single._1BreakingSafety

import org.junit.jupiter.api.Test
import kotlin.test.assertFailsWith

class TestNPE {
    @Test
    fun testThrowingNPE() {
        assertFailsWith<NullPointerException> {
            mightThrowNPE()
        }
    }
}
