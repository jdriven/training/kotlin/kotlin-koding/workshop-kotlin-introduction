package org.jetbrains.kotlinworkshop.student.single._1BreakingSafety;

import java.util.List;

public class JavaClass {
    public String getValue() {
        throw new UnsupportedOperationException();
    }

    public static void dangerousJavaMethod(List<Integer> list) {
        throw new UnsupportedOperationException();
    }
}
